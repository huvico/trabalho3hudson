/*
 * Peripheral_Setup.h
 *
 *  Created on: 17 de mar de 2021
 *      Author: Hudson
 */

#ifndef PERIPHERAL_SETUP_H_
#define PERIPHERAL_SETUP_H_
#include "F28x_Project.h"

void Setup_GPIO(void);
void Setup_PWM(void);
void Setup_ADC(void);
void Setup_DAC(void);





#endif /* PERIPHERAL_SETUP_H_ */
