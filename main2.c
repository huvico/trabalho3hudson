#include "Peripheral_Setup.h"
#include "math.h"
#include "MicroScope.h"

/**
 * main.c da CPU2
 */

float i_alpha = 0;
float i_alpha_last = 0;
float i_beta = 0;
float i_beta_last = 0;
float ia, ib, ic, va, vb, vc;
float L = 0.0029;
float R = 8.8;
float Ts = 166; //sample period in us
float Imax = 300;

float *p_V_alpha;
float *p_V_beta;


// Microscope  *****************************************************************************/
MicroScope scope;
float buffer[MS_NUMBER_OF_CHANNELS][MS_BUFFER_SIZE];
float bufferDisplay[MS_NUMBER_OF_CHANNELS][MS_BUFFER_SIZE];

// *****************************************************************************/

__interrupt void isr_cpu2_timer0(void);
__interrupt void isr_IPC2(void);

int main(void)
{
    InitSysCtrl();                          // Initialize System Control:
    DINT;                                   // Disable CPU interrupts
    InitPieCtrl();                          // Initialize the PIE control registers to their default state
    IER = 0x0000;                           // Disable CPU interrupts
    IFR = 0x0000;                           // Clear all CPU interrupt flags:
    InitPieVectTable();                     // Initialize the PIE vector table

    EALLOW;
    CpuSysRegs.PCLKCR0.bit.CPUTIMER0 = 1;

    PieVectTable.TIMER0_INT = &isr_cpu2_timer0;
    PieVectTable.IPC2_INT= &isr_IPC2;
    EDIS;


    PieCtrlRegs.PIEIER1.bit.INTx7 = 1;      //Timer 0
    PieCtrlRegs.PIEIER1.bit.INTx2 = 1;      //ADC B
    PieCtrlRegs.PIEIER1.bit.INTx15 = 1;     //IPC2 ISR
    IER |= M_INT1;

    InitCpuTimers();
    ConfigCpuTimer(&CpuTimer0, 200, Ts);

    while (IpcRegs.IPCSTS.bit.IPC5 == 0) ;  // Wait for CPU02 to set IPC4
    IpcRegs.IPCACK.bit.IPC4 = 1;

    Setup_DAC();

    IpcRegs.IPCSET.bit.IPC4 = 1;            // Let CPU1 know that CPU2 is ready, set IPC4 to release CPU1
    CpuTimer0Regs.TCR.all = 0x4001;
    EINT;                                   // Enable Global interrupt INTM
    ERTM;                                   // Enable Global realtime interrupt DBGM

    // initialize the microscope

               MS_init(&scope);
               scope.tm = TM_AUTO;
               scope.acq = MS_CONTINUOUS; //MS_CONTINUOUS;//MS_SINGLE;//
               scope.state = MS_WAITING;

               MS_connectChannel(&scope, 1, &va);

               MS_connectBuffer(&scope, 1, (float *)&buffer[0]);

               MS_connectOutputBuffer(&scope, 1, (float *)&bufferDisplay[0]);

               scope.preTrigger = 0;
               scope.pTrigger = &va;//TM_AUTO;


    while(1){


        ia = i_alpha_last*sqrtf(2/3);
        ib = -i_alpha_last*sqrtf(2/3)/2 + i_beta_last*sqrtf(2)/2;
        ic = -i_alpha_last*sqrtf(2/3)/2 - i_beta_last*sqrtf(2)/2;

        va = *p_V_alpha*sqrtf(2/3);
        vb = -*p_V_alpha*sqrtf(2/3)/2 + *p_V_beta*sqrtf(2)/2;
        vc = -*p_V_alpha*sqrtf(2/3)/2 - *p_V_beta*sqrtf(2)/2;
        MS_main(&scope);

    }
    return 0;
}

// Função para a interrupção do timer
__interrupt void isr_cpu2_timer0(void){
    GpioDataRegs.GPBTOGGLE.bit.GPIO34 = 1;


    i_alpha = (*p_V_alpha-i_alpha_last*R)*Ts/(L*1000000);
    i_beta = (*p_V_beta-i_beta_last*R)*Ts/(L*1000000);

    i_alpha_last += i_alpha;
    i_beta_last += i_beta;

    DacaRegs.DACVALS.bit.DACVALS = 3400*((Imax + i_alpha_last)/(2*Imax));
    DacbRegs.DACVALS.bit.DACVALS = 3400*((Imax + i_beta_last)/(2*Imax));

    MS_saveData(&scope);

    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1; // reconhece a interrupção limpando o flag IFR
    // importante limpar no inicio e verificar se houve outra interrupção no final do codigo
}

__interrupt void isr_IPC2(void){

    p_V_alpha = IpcRegs.IPCRECVADDR;
    p_V_beta = IpcRegs.IPCRECVCOM;
    IpcRegs.IPCACK.bit.IPC2 = 1;                // Manage the IPC registers, clear IPC1 flag
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;     // Must acknowledge the PIE group
}

