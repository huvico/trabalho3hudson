/*
 * Peripheral_Setup.c CPU2
 *
 *  Created on: 17 de mar de 2021
 *      Author: Hudson
 */


#include "Peripheral_Setup.h"

void Setup_GPIO(void){
// MESMO DO EXEMPLO GPIO SETUP DO C2000!!
    //
      // These can be combined into single statements for improved
      // code efficiency.
      //

      //
      // Enable PWM1-3 on GPIO0-GPIO1
      //
      EALLOW;

      //PWM 2A




      EDIS;

}

void Setup_PWM(void){
    EALLOW;
    CpuSysRegs.PCLKCR2.bit.EPWM2 = 1; // habilita o clock do pwm 7

    CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 0; // disable the sync clock
    //Setup PWM1 ============================================================================================
           EPwm2Regs.TBPRD = 5000; // Set the timer period
           EPwm2Regs.CMPA.bit.CMPA = EPwm2Regs.TBPRD >> 1;//set the width pulse
           EPwm2Regs.TBPHS.bit.TBPHS = 0; // set the phase, is zero, TBPHS=theta*TBPRD/360�
           EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO; // enable sinc pulse, this PWM is our ref, when CTR reachs zero
           EPwm2Regs.TBCTR = 0x0000; // reset the counter
           EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; //set up and down pwm type
           EPwm2Regs.TBCTL.bit.PHSEN = TB_DISABLE; // disable the signal sinc in, phase B and C have to config
           EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; //clock ratio to sysclkout
           EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1; // divide the clock, usual to slow pwm... like 20 Hz for example

           EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW; // set when the width is altered
           EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD;
           EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
           EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD;

           // sets when the PWM value is altered
           EPwm2Regs.AQCTLA.bit.PRD = AQ_NO_ACTION;
           EPwm2Regs.AQCTLA.bit.ZRO = AQ_NO_ACTION;
           EPwm2Regs.AQCTLA.bit.CAU = AQ_CLEAR;
           EPwm2Regs.AQCTLA.bit.CAD = AQ_SET;

           EPwm2Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE; // enable Dead-band module (turn-on delay + rise time)
           EPwm2Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC; // active hi complementary
           EPwm2Regs.DBFED.bit.DBFED = 80; // rise time FED = 20 TBCLKs
           EPwm2Regs.DBRED.bit.DBRED = 80; // fall
           //ativar o trigger do pwm
           EPwm2Regs.ETSEL.bit.SOCAEN = 1;//enable SOC on A group
           EPwm2Regs.ETSEL.bit.SOCASEL = ET_CTR_PRDZERO; // TRIGGER ZERO AND PRD, onde dispara o evento
           EPwm2Regs.ETPS.bit.SOCAPRD = ET_1ST; // trigger on every event


    CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 1;
    EDIS;

}

void Setup_ADC(void){
    Uint16 acqps;
    // determine minimum acquisition window (in SYSCLKS) based on resolution
    if(ADC_RESOLUTION_12BIT == AdcaRegs.ADCCTL2.bit.RESOLUTION)
        acqps = 14; //75 ns
    else
        acqps = 63;//320ns

    EALLOW;
    CpuSysRegs.PCLKCR13.bit.ADC_A = 1; //enable the clock of group ADC A
    AdcaRegs.ADCCTL2.bit.PRESCALE = 6; //defines the ADCCLK
    AdcSetMode(ADC_ADCA, ADC_RESOLUTION_12BIT, ADC_SIGNALMODE_SINGLE); //configures the adc
    AdcaRegs.ADCCTL1.bit.INTPULSEPOS = 1;// defines the pulse interruptions as 1 puls before
    AdcaRegs.ADCCTL1.bit.ADCPWDNZ = 1; // turns on the ADC
    DELAY_US(1000); // delay to power up the ADC

    //define the channel
    AdcaRegs.ADCSOC0CTL.bit.CHSEL = 3; // ADC IN 3
    AdcaRegs.ADCSOC0CTL.bit.ACQPS = acqps; // sample window
    AdcaRegs.ADCSOC0CTL.bit.TRIGSEL = TRIG_SEL_ePWM1_SOCA;
    //define the channe4
    AdcaRegs.ADCSOC1CTL.bit.CHSEL = 4; // ADC IN 3
    AdcaRegs.ADCSOC1CTL.bit.ACQPS = acqps; // sample window
    AdcaRegs.ADCSOC1CTL.bit.TRIGSEL = TRIG_SEL_ePWM1_SOCA;    // event that trigger the adc, vide table 11-33

    AdcaRegs.ADCINTSEL1N2.bit.INT1SEL = 0x01; //end of soc1 will se INT1 flag
    AdcaRegs.ADCINTSEL1N2.bit.INT1E = 1; // enable INT1 flag
    AdcaRegs.ADCINTFLGCLR.bit.ADCINT1 = 1; //clear INT1 flag

    EDIS;

}

void Setup_DAC(void){
    EALLOW;

    CpuSysRegs.PCLKCR16.bit.DAC_A = 1; //
    DacaRegs.DACCTL.bit.SYNCSEL = 0x01; //PWM 2
    DacaRegs.DACCTL.bit.LOADMODE = 0x01; //
    DacaRegs.DACCTL.bit.DACREFSEL = 0x01; // 0 a 3V
    DacaRegs.DACVALS.bit.DACVALS = 0; // 12bits
    DacaRegs.DACOUTEN.bit.DACOUTEN = 1;
    DacaRegs.DACLOCK.all = 0x00;

    CpuSysRegs.PCLKCR16.bit.DAC_B = 1; //
    DacbRegs.DACCTL.bit.SYNCSEL = 0x01; //PWM 2
    DacbRegs.DACCTL.bit.LOADMODE = 0x01; //
    DacbRegs.DACCTL.bit.DACREFSEL = 0x01; // 0 a 3V
    DacbRegs.DACVALS.bit.DACVALS = 0; // 12bits
    DacbRegs.DACOUTEN.bit.DACOUTEN = 1;
    DacbRegs.DACLOCK.all = 0x00;

    EDIS;

}
